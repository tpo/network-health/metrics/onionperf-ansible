#!/bin/sh
########################################################################
# bootstrap.sh - Bootstrap a new onionperf host for management with Ansible.
########################################################################

TARGET_OS=linux
LOGIN_USER=cloud

usage()
{
  echo "Usage: $0 [-u login_user] target"
  exit 2
}

bootstrap_linux()
{
    ssh $LOGIN_USER@$TARGET sudo apt install python3 sudo
    ANSIBLE_DEBUG=false ANSIBLE_VERBOSITY=3 \
    ansible-playbook \
      --limit $TARGET \
      --user $LOGIN_USER \
      --become-method sudo \
      --become-user root \
      site.yml
}

while getopts 'u:h' c
do
  case $c in
    u) LOGIN_USER=$OPTARG ;;
    h) usage ;;
  esac
done

shift $((OPTIND-1))

TARGET=$1

bootstrap_linux
